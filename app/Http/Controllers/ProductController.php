<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $matkul = array ('big data', 'cloud' , 'tata kelola TI', 'Enterprise Archiecture', 'IoT');
        return view('v_home', compact('matkul'));
    }
}
